# seata+eureka+feign+jpa

最近把老系统拆成了微服务，带来了分布式事务的问题，最终采用seata解决分布式事务的问题，采用的是AT模式，对业务侵入小，此文档是我的采坑记录



> 首先贴一个seata的官方文档

https://seata.io/zh-cn/docs/overview/what-is-seata.html



## 一、seata-server

### 1、seata-server的下载

https://github.com/seata/seata/releases

![image-20210825090354019](https://image-1259355241.cos.ap-shanghai.myqcloud.com/img/image-20210825090354019.png)

或者点击 [链接](https://shanghai-1259355241.file.myqcloud.com/file/seata-server-1.4.2.zip) 下载



解压打开就是这么个情况

![image-20210825092156926](https://image-1259355241.cos.ap-shanghai.myqcloud.com/img/image-20210825092156926.png)



### 2、修改配置文件

> 在seata-server中 只需要修改file.conf以及registry.conf就可以了

首先需要在seata连接的数据库中执行sql，创建seata-server需要的三张表

```sql
CREATE TABLE `branch_table` (
  `branch_id` bigint(20) NOT NULL,
  `xid` varchar(128) NOT NULL,
  `transaction_id` bigint(20) DEFAULT NULL,
  `resource_group_id` varchar(32) DEFAULT NULL,
  `resource_id` varchar(256) DEFAULT NULL,
  `branch_type` varchar(8) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `client_id` varchar(64) DEFAULT NULL,
  `application_data` varchar(2000) DEFAULT NULL,
  `gmt_create` datetime(6) DEFAULT NULL,
  `gmt_modified` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`branch_id`),
  KEY `idx_xid` (`xid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `global_table` (
  `xid` varchar(128) NOT NULL,
  `transaction_id` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `application_id` varchar(32) DEFAULT NULL,
  `transaction_service_group` varchar(32) DEFAULT NULL,
  `transaction_name` varchar(128) DEFAULT NULL,
  `timeout` int(11) DEFAULT NULL,
  `begin_time` bigint(20) DEFAULT NULL,
  `application_data` varchar(2000) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`xid`),
  KEY `idx_gmt_modified_status` (`gmt_modified`,`status`),
  KEY `idx_transaction_id` (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `lock_table` (
  `row_key` varchar(128) NOT NULL,
  `xid` varchar(96) DEFAULT NULL,
  `transaction_id` bigint(20) DEFAULT NULL,
  `branch_id` bigint(20) NOT NULL,
  `resource_id` varchar(256) DEFAULT NULL,
  `table_name` varchar(32) DEFAULT NULL,
  `pk` varchar(36) DEFAULT NULL,
  `gmt_create` datetime DEFAULT NULL,
  `gmt_modified` datetime DEFAULT NULL,
  PRIMARY KEY (`row_key`),
  KEY `idx_branch_id` (`branch_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```



#### 2.1  file.conf修改

![image-20210825092724058](https://image-1259355241.cos.ap-shanghai.myqcloud.com/img/image-20210825092724058.png)

>个人比较喜欢把globalTable、branchTable和lockTable三张表前面加个seata，方便查找。（当然这里改了上面sql的表明也要做出相应修改）



#### 2.2  registry.conf

![image-20210825092910138](https://image-1259355241.cos.ap-shanghai.myqcloud.com/img/image-20210825092910138.png)

> 这样配置文件就修改完成了



### 3、启动 seata-server

> 运行bin目录下的seata-server.bat（windwos）或者seata-server.sh（linux、mac）即可

![image-20210825093152433](https://image-1259355241.cos.ap-shanghai.myqcloud.com/img/image-20210825093152433.png)



> 只要eureka中存在seata-server这个服务seata-server就算启动成功

<img src="https://image-1259355241.cos.ap-shanghai.myqcloud.com/img/image-20210825093716254.png"/>





## 二、seata-client

### 1、引入依赖

>引入seata-serializer-kryo是因为springboot默认的序列化工具jackson会报错解析不了LocalDate,在github上的看到暂时还没有解决，所以一直使用seata提供的序列化工具kryo

```
 		<dependency>
            <groupId>io.seata</groupId>
            <artifactId>seata-spring-boot-starter</artifactId>
            <version>1.4.2</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-seata</artifactId>
            <version>2.2.1.RELEASE</version>
            <exclusions>
                <exclusion>
                    <groupId>io.seata</groupId>
                    <artifactId>seata-spring-boot-starter</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>io.seata</groupId>
            <artifactId>seata-serializer-kryo</artifactId>
            <version>1.4.2</version>
        </dependency>
```



### 2、更改配置文件

> 在application.yml中加入

````yaml
seata:
  enabled: true
  application-id: seata-server
  # tx-service-group 值必须要和vgroup-mapping下的key保持一直，不然会报错
  tx-service-group: bank-group 
  enable-auto-data-source-proxy: true
  use-jdk-proxy: false
  service:
    vgroup-mapping:
      bank-group: seata-server
    enable-degrade: false
    disable-global-transaction: false
  registry:
    type: eureka
    eureka:
      weight: 1
      service-url: ${eureka.client.service-url.defaultZone}
  client:
    undo:
      # 这里可以更改undo_log表名
      log-table: undo_log
      # 使用kryo是因为jackson会有一个localDate转换错误，seata暂时还没修复
      log-serialization: kryo
````



### 3、数据源手动代理

> 由于JPA的缘故，seata的自动注入好像不太好使，所以需要手动代理数据源。
>
> 但是，在seata关闭的时候代理数据源也会出问题，所以加了ConditionalOnProperty来监测seata是否启用

```java
package com.zcj.banka.config;

import com.alibaba.druid.pool.DruidDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @author zhouchj
 * @Date 2021/9/6
 **/
@Configuration
@ConditionalOnProperty(
        prefix = "seata",
        name = {"enabled"},
        havingValue = "true",
        matchIfMissing = true)
public class DataSourceConfig {


    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DruidDataSource druidDataSource() {
        return new DruidDataSource();
    }

    /**
     * 需要将 DataSourceProxy 设置为主数据源，否则事务无法回滚
     *
     * @param druidDataSource The DruidDataSource
     * @return The default datasource
     */
    @Primary
    @Bean("dataSource")
    public DataSource dataSource(DruidDataSource druidDataSource) {
        return new DataSourceProxy(druidDataSource);
    }
}

```







### 4、RequestInterceptor中新增（不必须）

> 如果你项目重写了feign的RequestInterceptor，然后遇到了XID不传送的问题，就需要在RequestInterceptor中加入以下代码

```java
/**
 * @author zhouchj
 */
@Configuration
public class UserFeignClientInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        // seata 传递XID
        String xid = RootContext.getXID();
        if (StringUtils.isNotEmpty(xid)) {
            template.header(RootContext.KEY_XID, xid);
        }
    }
}

```



### 5、加入注解启动事务

> 在方法上加上@GlobalTransactional注解即可启动全局事务，十分简单方便，对业务侵入少



## 三、demo

https://gitee.com/zhouacai/seata-eureka-feign-jpa