package com.zcj.banka.feign;

import com.zcj.banka.config.UserFeignClientInterceptor;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zhouchj
 * @Date 2021/8/25
 **/
@Component
@FeignClient(value = "bank-b", path = "/", configuration = UserFeignClientInterceptor.class)
public interface BankBFeign {

    @PutMapping("/bank-b/updAmount")
    void updAmount(@RequestParam("mode") String mode);
}
