package com.zcj.banka.service;

import com.zcj.banka.entity.AccountDO;
import com.zcj.banka.feign.BankBFeign;
import com.zcj.banka.repository.AccountRepository;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AccountService {

    private final AccountRepository accountRepository;

    private final BankBFeign bankBFeign;

    public AccountService(AccountRepository accountRepository,
                          BankBFeign bankBFeign) {
        this.accountRepository = accountRepository;
        this.bankBFeign = bankBFeign;
    }


    @GlobalTransactional()
    public void updAmount(String mode){
        AccountDO account = accountRepository.findById("1")
                .orElseThrow(()->new RuntimeException("用户未找到"));
        if("IN".equalsIgnoreCase(mode)){
            account.setAmount(account.getAmount() + 100);
            // 通过Feign向B银行服务转账
            bankBFeign.updAmount("OUT");
        }else{
            account.setAmount(account.getAmount() - 100);
            // 通过Feign向B银行服务转账
            bankBFeign.updAmount("IN");
        }
        account.setUpdateTime(new Date());
        accountRepository.saveAndFlush(account);

        if("OUT".equalsIgnoreCase(mode)){
            System.out.println(1/0);
        }
    }
}
