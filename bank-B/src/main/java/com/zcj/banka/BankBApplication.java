package com.zcj.banka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author 68300119
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
public class BankBApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankBApplication.class, args);
    }

}
