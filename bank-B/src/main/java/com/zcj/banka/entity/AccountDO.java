package com.zcj.banka.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name="account")
@Entity
@Data
@NoArgsConstructor
public class AccountDO implements Serializable{

    /**
     * id
     */
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	private String id;

    /**
     * 用户ID
     */
    @Column(name = "userid")
	private String userid;

    /**
     * 用户名称
     */
    @Column(name = "username")
	private String username;

    /**
     * 账户余额
     */
    @Column(name = "amount")
	private Integer amount;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
	private Date updateTime;
}
