package com.zcj.banka.repository;

import com.zcj.banka.entity.AccountDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author zhouchj
 * @Date 2021/8/25
 **/
@Repository
public interface AccountRepository extends JpaRepository<AccountDO, String>, JpaSpecificationExecutor<AccountDO> {

}
