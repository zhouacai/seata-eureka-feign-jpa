package com.zcj.banka.service;

import com.zcj.banka.entity.AccountDO;
import com.zcj.banka.repository.AccountRepository;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class AccountService {

    private final AccountRepository accountRepository;


    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }


    @GlobalTransactional
    public void updAmount(String mode){
        AccountDO account = accountRepository.findById("1")
                .orElseThrow(()->new RuntimeException("用户未找到"));
        if("IN".equalsIgnoreCase(mode)){
            account.setAmount(account.getAmount() + 100);
            // 通过Feign向B银行服务转账
        }else{
            account.setAmount(account.getAmount() - 100);
            // 通过Feign向B银行服务转账
        }
        account.setUpdateTime(new Date());
        accountRepository.saveAndFlush(account);
    }
}
